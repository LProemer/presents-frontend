import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Observable } from 'rxjs';


export interface User {
  name: string;
  id: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  createUser(name: string): Observable<User> {
    return this.http.post<User>(environment.apiUrl + 'user', name);
  }

getUser(userId: string) {
  return this.http.get<User>(environment.apiUrl + 'user/'+ userId);
}

}
