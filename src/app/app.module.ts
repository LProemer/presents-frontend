import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';


import { AppComponent } from './app.component';
import { PresentListComponent } from './present-list/present-list.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { PresentAddComponent } from './present-add/present-add.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UserAddComponent } from './user-add/user-add.component';

const appRoutes: Routes = [
  {
    path: 'start',
    component: UserAddComponent
  },
  {
    path: 'user/:id',
    component: PresentListComponent
  },
  {
    path: '',
    redirectTo: '/start',
    pathMatch: 'full'
  },
  //{ path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    PresentListComponent,
    PresentAddComponent,
    UserAddComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
