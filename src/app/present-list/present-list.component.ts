import { Component, OnInit } from '@angular/core';
import { PresentService, Config } from '../present/present.service';
import { ActivatedRoute } from '@angular/router';
import { UserService, User } from '../user/user.service';

@Component({
  selector: 'app-present-list',
  templateUrl: './present-list.component.html',
  styleUrls: ['./present-list.component.sass'],
  providers: [PresentService, UserService],
})


export class PresentListComponent implements OnInit {

  config: Config[];
  user: User;

  constructor(private presentService: PresentService, private userService: UserService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      let userId = params.get('id');
      this.userService.getUser(userId)
        .subscribe(user => this.user = user);
    });
  }

  showConfig() {

    this.presentService.getConfig()
      .subscribe((data: Config[]) => this.config = data);
  }


}
