import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { catchError } from 'rxjs/operators/catchError';

//TODO: rename to present!
export interface Config {
  name: string;
}

@Injectable()
export class PresentService {


  constructor(private http: HttpClient) { }

  getConfig() {
    return this.http.get<Config[]>(environment.apiUrl + 'present');
  }

  createPresent(present: Config) {
    console.log("posting");
    this.http.post<Config>(environment.apiUrl + 'present', present).subscribe();
  }

}
